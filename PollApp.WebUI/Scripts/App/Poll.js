﻿/// <reference path="../knockout-2.2.1.debug.js" />

(function () {
    "use strict";

    function PollModel(modelData) {
        var self = this;

        self.Name = ko.observable(modelData.Name);
        self.StepNo = ko.observable(1);
        self.StepCount = ko.observable(modelData.Questions.length);
        self.Answers = ko.observableArray();
        self.IsRequiredWarningVisible = ko.observable(false);
        self.PollResult = ko.observable();
        self.ActiveQuestion = {
            Text: ko.observable(),
            Type: ko.observable(""),
            Required: ko.observable(),
            Options: ko.observableArray(),

            SelectedOption: ko.observable(), //this is for select type
            TypedText: ko.observable(), //this is for text type
            SelectedOptions: ko.observableArray() //this is for multiselect type
        };
        
        self.ActiveQuestion.IsText = ko.computed(function () {
            return self.ActiveQuestion.Type() == "Text";
        });
        self.ActiveQuestion.IsSelect = ko.computed(function () {
            return self.ActiveQuestion.Type() == "Select";
        });
        self.ActiveQuestion.IsMultiSelect = ko.computed(function () {
            return self.ActiveQuestion.Type() == "MultiSelect";
        });
        
        setQuestion(0);

        function setQuestion(index) {
            var q = modelData.Questions[index];
            self.ActiveQuestion.Text(q.Text);
            self.ActiveQuestion.Type(q.Type);
            self.ActiveQuestion.Required(q.Required);
            var options = q.Options;
            if (q.Type == "Select") {   //add empty option
                options = [].concat([{ Key: '', Value: '' }], options)
            }
            options = ko.utils.arrayMap(options, function (el) {
                return {
                    Key: el.Key,
                    Value: el.Value,
                    Checked: ko.observable(false)
                };
            });
            self.ActiveQuestion.Options(options);
            self.ActiveQuestion.SelectedOption(null);
            self.ActiveQuestion.TypedText(null);
        }

        self.NextClick = function () {
            //gather results
            var question = modelData.Questions[self.StepNo() - 1];
            var qData = {
                StepNo: self.StepNo(),
                QuestionId: question.Id,
                QuestionText: question.Text,
                QuestionType: question.Type,
                Data: '',
                Value: ''
            };
            if (self.ActiveQuestion.IsText()) {
                qData.Data = self.ActiveQuestion.TypedText();
                qData.Value = qData.Data;
            }
            if (self.ActiveQuestion.IsSelect()) {
                qData.Data = self.ActiveQuestion.SelectedOption().Key + '';
                qData.Value = self.ActiveQuestion.SelectedOption().Value;
            }
            if (self.ActiveQuestion.IsMultiSelect()) {
                var checked = ko.utils.arrayFilter(self.ActiveQuestion.Options(), function (op) {
                    return op.Checked();
                });
                qData.Data = ko.utils.arrayMap(checked, function (el) { return el.Key; }).join(", ");
                qData.Value = ko.utils.arrayMap(checked, function (el) { return el.Value; }).join(", ");
            }

            if (question.Required && ko.utils.stringTrim(qData.Data) == '') {
                self.IsRequiredWarningVisible(true);
                return;
            }

            self.IsRequiredWarningVisible(false);
            self.Answers.push(qData);

            self.StepNo(self.StepNo() + 1);
            var no = self.StepNo();
            if (no <= modelData.Questions.length) {
                setQuestion(no - 1);
            }
        }

        self.IsShowFinish = ko.computed(function () {
            return self.StepNo() > self.StepCount();
        });

        self.SaveClick = function () {
            var result = {
                PollId: modelData.Id,
                ClientId: modelData.ClientId,
                Answers: ko.utils.arrayMap(self.Answers(), function (a) {
                    return {
                        QuestionId: a.QuestionId,
                        QuestionType: a.QuestionType,
                        Data: a.Data
                    }
                })
            }

            self.PollResult(JSON.stringify(result));
            return true; //continue to submit
        };
    }

    ko.applyBindings(new PollModel(window.pollModel));
})();