﻿using PollApp.Data.Models;
using PollApp.Data.Repositories.EFPollsRepository;
using PollApp.WebUI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PollApp.WebUI.Controllers
{
    public class PollsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult Poll(string friendlyId)
        {
            var pollsRepo = new EFPollsRepository();
            var poll = pollsRepo.GetPollDataByFriendlyId(friendlyId);
            var modelBuilder = new PollViewModelBuilder();
            var model = modelBuilder.BuildViewModel(poll);

            return View(model);
        }

        [HttpPost]
        public ActionResult SavePollFormSubmit(string pollResult)
        {
            // Manually deserialize submitted data - it is html encoded json
            var result = JsonConvert.DeserializeObject<PollResultViewModel>(HttpUtility.HtmlDecode(pollResult));

            // Transform view model to Data entities
            var dataModelBuilder = new AnswersDataModelBuilder();
            var answers = dataModelBuilder.BuildAnswersDataModel(result);

            // Save to DB
            var pollsRepo = new EFPollsRepository();
            pollsRepo.SavePollAnswers(answers);

            return RedirectToAction("Confirmation", new { clientId = result.ClientId });
        }

        public ActionResult Confirmation(Guid clientId)
        {
            return View();
        }
    }
}
