﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PollApp.WebUI.Models
{
    public class PollResultViewModel
    {
        public long PollId { get; set; }

        public Guid ClientId { get; set; }

        public List<PollResultItem> Answers { get; set; }

        public class PollResultItem
        {
            public string QuestionId { get; set; }
            public string QuestionType { get; set; }
            public string Data { get; set; }
        }
    }
}