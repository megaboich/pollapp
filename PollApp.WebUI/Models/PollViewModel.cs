﻿using PollApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PollApp.WebUI.Models
{
    public class PollViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<QuestionModel> Questions { get; set; }
        public Guid ClientId { get; set; }

        public class QuestionModel
        {
            public long Id { get; set; }
            public bool Required { get; set; }
            public string Type { get; set; }
            public string Text { get; set; }
            public List<KeyValuePair<string, string>> Options { get; set; }
        }
    }
}