﻿using PollApp.Data.Models;
using PollApp.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PollApp.WebUI.Models
{
    public class PollViewModelBuilder
    {
        public PollViewModel BuildViewModel(Poll poll)
        {
            //fill view model
            var model = new PollViewModel
            {
                Id = poll.Id,
                Name = poll.Name,
                ClientId = Guid.NewGuid(),
                Questions = poll.Questions
                    .OrderBy(q => q.OrderIndex)
                    .Select(q => new PollViewModel.QuestionModel
                    {
                        Id = q.Id,
                        Required = q.Required,
                        Text = q.Text,
                        Type = q.Type.ToString(),
                        Options = q.Options != null 
                            ? q.Options
                                .OrderBy(o => o.OrderIndex)
                                .Select(o => new KeyValuePair<string, string>(o.Id.ToString(), o.Text))
                                .ToList()
                            : new List<KeyValuePair<string, string>>(),
                    })
                    .ToList()
            };

            return model;
        }
    }
}