﻿using PollApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PollApp.WebUI.Models
{
    public class AnswersDataModelBuilder
    {
        public List<Answer> BuildAnswersDataModel(PollResultViewModel resultViewModel)
        {
            var answers = new List<Answer>();
            foreach (var a in resultViewModel.Answers)
            {
                var type = (QuestionType)Enum.Parse(typeof(QuestionType), a.QuestionType);
                var data = (type == QuestionType.MultiSelect)
                    ? a.Data.Split(',').Select(s => s.Trim()).ToArray() //Split up values in data for multiselect to create different Answer records in DB
                    : new[] { a.Data };

                foreach (var d in data)
                {
                    answers.Add(new Answer
                    {
                        ClientId = resultViewModel.ClientId,
                        Data = d,
                        PollId = resultViewModel.PollId,
                        QuestionId = long.Parse(a.QuestionId)
                    });
                }
            }

            return answers;
        }
    }
}