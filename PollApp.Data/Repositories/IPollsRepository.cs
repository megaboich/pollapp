﻿using PollApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollApp.Data.Repositories
{
    public interface IPollsRepository
    {
        Poll GetPollDataByFriendlyId(string friendlyId);

        void SavePollAnswers(IEnumerable<Answer> answers);
    }
}
