﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollApp.Data.Repositories.EFPollsRepository
{
    public class EFPollsRepository : IPollsRepository
    {
        PollsDb _db;

        public EFPollsRepository()
        {
            _db = new PollsDb();
        }

        public Models.Poll GetPollDataByFriendlyId(string friendlyId)
        {
            var poll = _db.Polls
                .Include("Questions")
                .Include("Questions.Options")
                .SingleOrDefault(p => p.FriendlyId == friendlyId);

            if (poll == null)
            {
                throw new Exception("Poll not found");
            }

            return poll;
        }


        public void SavePollAnswers(IEnumerable<Models.Answer> answers)
        {
            foreach(var a in answers) 
            {
                _db.Answers.Add(a);
            }

            _db.SaveChanges();
        }
    }
}
