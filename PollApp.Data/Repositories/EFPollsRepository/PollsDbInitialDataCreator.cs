﻿using PollApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollApp.Data.Repositories.EFPollsRepository
{
    public class PollsDbInitialDataCreator : DropCreateDatabaseIfModelChanges<PollsDb>
    {
        protected override void Seed(PollsDb db)
        {
            var poll = new Poll
            {
                Name = "Test poll",
                FriendlyId = "test",
                Questions = new[] {
                        new Question 
                        { 
                            OrderIndex = 1,
                            Required = true, 
                            Text = "What is your age?", 
                            Type = QuestionType.Select,
                            Options = new[] {
                                new Option { Text = "Infant" },
                                new Option { Text = "Child" },
                                new Option { Text = "Teenager" },
                                new Option { Text = "Adult" },
                            }.ToList()
                        },
                        new Question
                        {
                            OrderIndex = 2,
                            Required = false, 
                            Text = "What colors do you like?", 
                            Type = QuestionType.MultiSelect,
                            Options = new[] {
                                new Option { Text = "Red" },
                                new Option { Text = "Green" },
                                new Option { Text = "Black" },
                                new Option { Text = "Yellow" },
                                new Option { Text = "Blue" },
                            }.ToList()
                        },
                        new Question
                        {
                            OrderIndex = 3,
                            Required = true, 
                            Text = "What is your favorite music?", 
                            Type = QuestionType.Text,
                        },
                    }.ToList()
            };

            var poll2 = new Poll
            {
                Name = "Test adwanced poll",
                FriendlyId = "advanced-test",
                Questions = new[] {
                        new Question 
                        { 
                            OrderIndex = 1,
                            Required = true, 
                            Text = "Single select - required", 
                            Type = QuestionType.Select,
                            Options = new[] {
                                new Option { Text = "option 1" },
                                new Option { Text = "option 2" },
                                new Option { Text = "option 3" },
                                new Option { Text = "option 4" },
                            }.ToList()
                        },
                        new Question
                        {
                            OrderIndex = 2,
                            Required = true, 
                            Text = "Multi select - required", 
                            Type = QuestionType.MultiSelect,
                            Options = new[] {
                                new Option { Text = "multi option 1" },
                                new Option { Text = "multi option 2" },
                                new Option { Text = "multi option 3" },
                                new Option { Text = "multi option 4" },
                                new Option { Text = "multi option 5" },
                                new Option { Text = "multi option 6" },
                            }.ToList()
                        },
                        new Question
                        {
                            OrderIndex = 3,
                            Required = true, 
                            Text = "Text field - required", 
                            Type = QuestionType.Text,
                        },
                        new Question 
                        { 
                            OrderIndex = 4,
                            Required = false, 
                            Text = "Single select - not required", 
                            Type = QuestionType.Select,
                            Options = new[] {
                                new Option { Text = "option 1" },
                                new Option { Text = "option 2" },
                                new Option { Text = "option 3" },
                                new Option { Text = "option 4" },
                            }.ToList()
                        },
                        new Question
                        {
                            OrderIndex = 5,
                            Required = false, 
                            Text = "Multi select - not required", 
                            Type = QuestionType.MultiSelect,
                            Options = new[] {
                                new Option { Text = "multi option 1" },
                                new Option { Text = "multi option 2" },
                                new Option { Text = "multi option 3" },
                                new Option { Text = "multi option 4" },
                                new Option { Text = "multi option 5" },
                                new Option { Text = "multi option 6" },
                            }.ToList()
                        },
                        new Question
                        {
                            OrderIndex = 6,
                            Required = false, 
                            Text = "Text field - not required", 
                            Type = QuestionType.Text,
                        },
                    }.ToList()
            };

            db.Polls.Add(poll);
            db.Polls.Add(poll2);
            db.SaveChanges();


        }

    }

}
