﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollApp.Data.Models
{
    public class Answer
    {
        public long Id { get; set; }

        public Guid ClientId { get; set; }

        public long PollId { get; set; }

        public long QuestionId { get; set; }

        /// <summary>
        /// This data is a string representation of answer depending of question type.
        /// For select questions it is Id of selected option, for text type it is entire text
        /// </summary>
        public string Data { get; set; }

        
    }
}
