﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollApp.Data.Models
{
    public class Option
    {
        public long Id { get; set; }

        public long QuestionId { get; set; }

        public string Text { get; set; }

        public int OrderIndex { get; set; }
    }
}
