﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollApp.Data.Models
{
    public class Poll
    {
        public long Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Used to buid Url to poll page
        /// </summary>
        public string FriendlyId { get; set; }

        public ICollection<Question> Questions { get; set; }
    }
}
