﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollApp.Data.Models
{
    public enum QuestionType
    {
        MultiSelect = 0,
        Select = 1,
        Text = 2
    }

    public class Question
    {
        public long Id { get; set; }

        public long PollId { get; set; }

        public bool Required { get; set; }

        public int OrderIndex { get; set; }

        public QuestionType Type { get; set; }

        public string Text { get; set; }

        /// <summary>
        /// This options are only for select and multiselect types. It is not needed for text types.
        /// </summary>
        public ICollection<Option> Options { get; set; }
    }
}
